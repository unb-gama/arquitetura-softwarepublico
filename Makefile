
BASE_NAME = arquiteturaSoftware-SPB-ASw

LATEX     = latex
PDFLATEX  = pdflatex
BIBTEX    = bibtex
MAKEINDEX = makeindex

SOURCES = 00-resumo.tex 01-definicao.tex 02-visaoGeral.tex 03-instanciacao.tex\
		04-noosfero.tex 05-mezuro.tex 06-analizo.tex 07-infraestrutura.tex

pdf: $(BASE_NAME).pdf
	evince $(BASE_NAME).pdf &
ps: $(BASE_NAME).ps

$(BASE_NAME).pdf: $(BASE_NAME).tex $(SOURCES)
	$(PDFLATEX) $(BASE_NAME).tex
	$(BIBTEX) $(BASE_NAME)
	$(MAKEINDEX) $(BASE_NAME)
	$(PDFLATEX) $<
	$(PDFLATEX) $<

$(BASE_NAME).ps: $(BASE_NAME).tex
	$(LATEX) $<
	$(BIBTEX) $(BASE_NAME)
	$(MAKEINDEX) $(BASE_NAME)
	$(LATEX) $<
	$(LATEX) $<

clean:
	rm -f $(BASE_NAME)*.ps $(BASE_NAME)*.dvi *.log \
	      *.aux *.blg *.toc *.brf *.ilg *.ind \
	      missfont.log $(BASE_NAME)*.bbl $(BASE_NAME)*.pdf $(BASE_NAME)*.out \
		  $(BASE_NAME)*.lof $(BASE_NAME)*.lot  *.swp
