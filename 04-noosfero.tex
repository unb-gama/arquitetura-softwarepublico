\section{Rede Social: Noosfero}
\label{noosfero}

O Noosfero \footnote{\url{http://www.noosfero.org}} é uma plataforma \textit{web} livre para criação de redes sociais com \textit{blog}, e-Portifólios, CMS, RSS, discussão temática, agenda de eventos, galeria de imagens, \textit{chat}, entre outros. Ele foi desenvolvido pela Cooperativa de Tecnologias Livres - Colivre \footnote{\url{(http://www.colivre.coop.br}} em 2007, sob a licença AGPL v.3, com a proposta de permitir ao usuário criar sua própria rede social personalizada, livre e autônoma.

O Noosfero é desenvolvido em linguagem Ruby \footnote{\url{https://www.ruby-lang.org/en/}}, usando o \textit{framework} para aplicações \textit{web Rails} \footnote{\url{http://rubyonrails.org/}}.  A arquitetura do \textit{Ruby on Rails} possui as seguintes características:

\begin{itemize}
	\item Utiliza o \textit{framework} \textit{Model-View-Controller} (MVC);
	\item Representação de Transferência de Estados (REST) para serviços \textit{web};
\item Suporta os principais banco de dados (MySQL, PostgreSQL, Oracle, entre outros);
	\item Linguagem de  \textit{script server-side} (transforma  \textit{websites} de HTML estática em dinâmica).
\end{itemize}

\subsection{Framework Model-View-Controller}

	A Figura \ref{noosfero_arq} abaixo apresenta uma visão arquitetural de alto nível do núcleo do Noosfero:

	\begin{figure}[!htb]
	\centering
		\includegraphics[keepaspectratio=true,scale=0.6]{figuras/noosfero.jpg}
	\caption{Noosfero: Arquitetura}
	\label{noosfero_arq}
	\end{figure}

O padrão arquitetural utilizado é o \textit{Model-View-Controller}, visando melhorar a manutenibilidade do sistema. A \textit{Model} está relacionada com a lógica do domínio e as tabelas do banco de dados, a \textit{View} gerencia a lógica de exibição do conteúdo e a \textit{Controller} lida com o fluxo da aplicação.

\begin{itemize}
	\item \textbf{Browser ou Cliente:}  O cliente ou  \textit{browser} faz uma requisição ao servidor, solicitando um conteúdo que está procurando (URL).
	\item \textbf{Servidor Web:} No Noosfero este servidor é o Thin, utilizado para processar as requisições de entrada e saída. Pode ser configurado para utilizar mais de um processo para realizar balanceamento de carga. É recomendável o uso de dois processos do Thin por núcleo de processador do servidor hospedeiro. Porém antes da requisição chegar nesta etapa, o Noosfero também possui um acelerador de aplicações \textit{web} conhecido como \textit{cache} de \textit{proxy} reverso HTTP (para acessar conteúdo estático).  Além disso, o \textit{Apache} é usado como servidor de \textit{proxy} reverso.
\item \textbf{Dispatcher:} Chamado de \textit{ActionDispatch}, funciona como um roteador, em que sua função é mapear as requisições que chegam às ações de suas respectivas \textit{controllers}, dependendo da requisição HTTP. Estas ações de controladoras representam a implementação do padrão de arquitetura REST (\textit{Representational State Transfer}).  Exemplo de roteamento pode ser visto na Tabela \ref{rotas}.
\begin{table}[h]
	\centering
	\caption{ Rotas}
	\label{rotas}
	\begin{tabularx}{0.95\textwidth}{lllX}
		\toprule
		\textbf{Requisição HTTP} & \textbf{URL} & \textbf{Ação} & \textbf{Propósito} \\
		\midrule
		GET & /users & index &  Página para listar todos os usuário\\
    \rowcolor[gray]{0.9}
		GET & /users/1 & show &  Página para mostrar usuário com identificador igual a 1 \\
		GET & /users/new & new &  Página para fazer um novo usuário \\
    \rowcolor[gray]{0.9}
		POST & /users & create & Criar um novo usuário \\

		GET & /users/1/edit & edit & Página para editar usuário com identificador igual a 1 \\
    \rowcolor[gray]{0.9}
		PATCH & /users/1 & update & Atualizar usuário com identificador igual a 1 \\

		DELETE & /users/1 & destroy & Deletar usuário com identificador igual a 1  \\
\bottomrule
	\end{tabularx}
\end{table}

Este mapeamento pode ser encontrado no arquivo \textit{config/routes.rb}.
\item \textbf{Controller:} Controla o fluxo da aplicação, realizando a ligação entre as entidades de \textit{model} e \textit{view} através de chamadas de métodos. Faz a consulta dos dados necessários na \textit{model} e em seguida chama a respectiva \textit{view} para realizar a renderização.
\item \textbf{Model:} Representa as entidades de domínio da aplicação.  A lógica da aplicação é implementada nas classes de \textit{model}, que herdam da classe-base \textit{ActiveRecord}. Ela é utilizada para conectar as tabelas de banco de dados relacionais com suas respectivas representações em classes \textit{ruby}. Também provê procuras e cria relacionamentos ou associações entre os modelos.
\item \textbf{View:} Chamada pela \textit{controller}, é responsável pela visualização das páginas, isto é, as saídas em HTML da aplicação. A classe-base ActionView provê \textit{layouts}, \textit{templates} e \textit{helpers} que assistem na geração do HTML ou outros formatos. Existem três esquemas em Rails: rhtml, rxml e rjs. O rhtml gera HTML \textit{views}  para usuários com ERB (código Ruby embutido em HTML). O rxml é usado para construir documentos XML com Ruby e o rjs permite criar código \textit{JavaScript}.

\end{itemize}

  \subsection{Entidades de Domínio}

O Noosfero é uma plataforma multi-ambiente, o que significa que se pode ter duas redes sociais distintas, com domínios, usuários e comunidades distintas em uma mesma instalação, conforme a Figura \ref{dominio_principal}.

	 \begin{figure}[!htb]
	\centering
		\includegraphics[keepaspectratio=true,scale=0.5]{figuras/domain_main.jpg}
	\caption{Modelo Domínio: Principal}
	\label{dominio_principal}
	\end{figure}

A entidade \textit{Profile}, em português Perfil, é uma abstração das três formas de entidades concretas de perfil existentes no Noosfero: pessoa, comunidade e empreendimento (em inglês: \textit{person}, \textit{community} e \textit{enterprise}).
Em outras palavras, as três entidades possuem características em comum e são tratadas como uma só em determinados contextos dentro da aplicação. Existe ainda outra entidade que abstraí o comportamento comum a comunidades e empreendimentos, chamada de organização (em inglês, \textit{organization}).

A Figura  \ref{dominio_perfis} apresenta a relação entre os tipos de perfil. A seta com a cabeça triangular representa uma relação de herança.

 	\begin{figure}[!htb]
	\centering
		\includegraphics[keepaspectratio=true,scale=0.5]{figuras/domain_profiles.jpg}
	\caption{Modelo Domínio: Perfis}
	\label{dominio_perfis}
	\end{figure}

Existe ainda a entidade \textit{User}, ou Usuário, que é mantida separada da entidade Pessoa por questões de \textit{design} do código da aplicação, que é quem implementa a lógica de autenticação da aplicação. Desta forma a lógica de autenticação fica separada da lógica de visualização e personalização do perfil.

Por fim, as entidades mostradas na Figura \ref{dominio_artigos} representam os principais tipos de conteúdos disponíveis no Noosfero, artigos de texto, pastas, \textit{blogs}, galerias de imagens, arquivos e \textit{feeds} de notícias, assim como a relação de herança entre estes.

	\begin{figure}[!htb]
	\centering
		\includegraphics[keepaspectratio=true,scale=0.5]{figuras/domain_profiles.jpg}
	\caption{Modelo Domínio: Artigos}
	\label{dominio_artigos}
	\end{figure}

\subsection{\textit{Plugins}}

A arquitetura do Noosfero foi pensada para permitir que seja facilmente expansível, de forma que as funcionalidades que não sejam comuns ao conceito de redes sociais sejam desenvolvidas como \textit{plugins}, diminuindo o acoplamento e aumentando a coesão das funcionalidades do sistema.
Uma das grandes vantagens em se criar uma aplicação com arquitetura extensível é a possibilidade de criar \textit{plugins} sem precisar modificar o código fonte do núcleo da ferramenta, além de permitir o isolamento de \textit{bugs} encontrados mais facilmente.

Os mantenedores do Noosfero, responsáveis por aprovar solicitações de alteração no código, exigem que os \textit{plugins} tenham um certo nível de testes para que esses possam ser incorporados à uma versão do Noosfero, evitando a inserção de \textit{bugs} que afetem o core ou outros \textit{plugins}, além de manter o padrão de qualidade de código.
Assim, é possível ter diferentes instâncias do Noosfero - que utilizam o mesmo núcleo em comum - mas que diferem no uso de \textit{plugins} com funcionalidades próprias às suas necessidades específicas.

A estrutura de \textit{plugins} do Noosfero é construída em cima do paradigma orientado a eventos, em que um evento (chamado de \textit{hotspots}) é ativado em um determinado ponto e todos os \textit{plugins} interessados neste evento são capazes de agir. Dentro deste contexto, há dois papéis básicos: o do desenvolvedor do Noosfero e o do \textit{plugin}.

Na criação de um \textit{plugin}, o desenvolvedor do Noosfero deve criar os \textit{hotspots} nos quais os \textit{plugins} podem se registrar e realizar novas funcionalidades. Já no lado do \textit{plugin}, o evento será sobrescrito pela funcionalidade desejada (respeitando a especificação). Esta nova informação gerada pelo \textit{plugin} é interpretada pelo Noosfero, através de um Gerenciador. Ele é recarregado em toda nova requisição com instâncias de cada \textit{plugin}. Este Gerenciador pode ser acessado em toda \textit{view} e \textit{controller} por meio da variável @plugins. Além disso também há um método chamado \textit{map}  que recebe um símbolo especificando o evento a ser alertado e retorna uma lista com as respostas de todos os plugins relacionados a aquele evento.

A ideia pode ser resumida na Figura \ref{plugin} e para mais informações sobre os \textit{plugins}, consultar a documentação do Noosfero \footnote{\url{http://noosfero.org/Development/PluginsArchitecture}}.
\\\\\\\\\\\\\\\\
	\begin{figure}[!htb]
	\centering
		\includegraphics[keepaspectratio=false,scale=0.6]{figuras/plugin.jpg}
	\caption{\textit{Plugins}}
	\label{plugin}
	\end{figure}



